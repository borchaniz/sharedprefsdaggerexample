package com.borch.daggerprefs

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentManager
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class Act2 : DaggerAppCompatActivity(), MadeByFragment.OnFragmentInteractionListener {

    @Inject
    lateinit var madeByFragment: MadeByFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_act2)
        supportFragmentManager.beginTransaction().add(R.id.ll,madeByFragment).commit()
    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
