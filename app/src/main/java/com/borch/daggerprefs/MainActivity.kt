package com.borch.daggerprefs

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.util.Log
import android.widget.Button
import android.widget.EditText
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), MadeByFragment.OnFragmentInteractionListener {
    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Inject
    lateinit var preferences: SharedPreferences

    @Inject
    lateinit var madeByFragment: MadeByFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<EditText>(R.id.editText).setText(preferences.getString("testKey", ""))
        findViewById<Button>(R.id.button).setOnClickListener {
            preferences
                    .edit()
                    .putString("testKey", findViewById<EditText>(R.id.editText).text.toString())
                    .apply()
        }
        supportFragmentManager.beginTransaction().add(R.id.linearLayout,madeByFragment).commit()

        findViewById<Button>(R.id.act2Call).setOnClickListener{
            supportFragmentManager.beginTransaction().remove(madeByFragment).commit()
            startActivity(Intent(this,Act2::class.java))
        }

    }
}
