package com.borch.daggerprefs.di

import android.app.Application
import com.borch.daggerprefs.App
import com.borch.daggerprefs.MadeByFragment
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            AndroidSupportInjectionModule::class,
            SharedPreferencesModule::class,
            BindingModule::class]
)
interface SharedPreferencesComponent : AndroidInjector<App> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun create(app:Application): Builder

        fun build(): SharedPreferencesComponent
    }
}