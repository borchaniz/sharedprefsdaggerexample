package com.borch.daggerprefs.di

import com.borch.daggerprefs.Act2
import com.borch.daggerprefs.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BindingModule {
    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun act2():Act2

}