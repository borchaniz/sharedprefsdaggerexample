package com.borch.daggerprefs.di

import android.app.Application
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.borch.daggerprefs.MadeByFragment
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class SharedPreferencesModule {

    @Provides
    @Singleton
    fun provideSharedPreferences(app: Application): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(app)

    @Provides
    @Singleton
    fun provideMadeByFragment() = MadeByFragment()
}
